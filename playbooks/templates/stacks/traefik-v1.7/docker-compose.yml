version: '3.7'

services:
  traefik:
    image: traefik:v1.7.22-alpine
    command: > 
      traefik 
      --docker 
      {% if swarm_enabled %}
      --docker.swarmmode 
      {% endif %}
      --docker.watch 
      --docker.exposedbydefault=false 
      --insecureSkipVerify 
      --entrypoints="Name:http Address::80" 
      --entrypoints="Name:https Address::443 TLS" 
      --metrics
      --metrics.prometheus
      {% if letsenctrypt_enabled|bool %}
      --acme 
      --acme.email={{ letsencrypt_mail }} 
      --acme.storage="/etc/traefik/acme.json" 
      --acme.entryPoint=https 
      --acme.dnsChallenge.provider=cloudflare 
      --acme.dnsChallenge.resolvers="1.1.1.1:53" 
      --acme.onhostrule=true 
      --acme.acmelogging=true 
      {% endif %}
      --logLevel=INFO 
      --api 
      --docker.domain={{ base_domain }} 
      --ping 
      --rest 
      --retry 
      --configFile=/config/traefik.toml
    environment:
      {% if cloudflare_enabled|bool %}
      CF_API_EMAIL: "{{ cloudflare_api_email }}"
      CF_API_KEY: "{{ cloudflare_api_key }}"
      {% endif %}
    networks:
      - proxy
    ports:
      - 80:80
      - 443:443
    volumes: 
      - traefik-config:/etc/traefik
      - /var/run/docker.sock:/var/run/docker.sock:ro
    deploy:
      update_config:
          parallelism: 1
          delay: 30s
          monitor: 30s
          order: stop-first
          failure_action: rollback
      restart_config:
          condition: on-failure
      placement:
          constraints: 
          - node.role == manager
      labels:
          traefik.enable: "true"
          traefik.tags: "proxy"
          traefik.docker.network: "proxy"
          traefik.port: "8080"
          traefik.frontend.rule: "Host:proxy.{{ base_domain }}"
          traefik.backend: "traefik"
          traefik.protocol: "http"
          traefik.frontend.headers.SSLRedirect: "true"
          traefik.frontend.entryPoints: "http,https"
          traefik.backend.loadbalancer.stickiness: "true"
          traefik.frontend.auth.basic.users: "{{admin_user}}:{{admin_password_hash.stdout}}"

networks:
  proxy:
    driver: overlay
    attachable: true
    name: proxy

volumes:
  traefik-config:
    {% if storidge_enabled|bool %}
    driver_config: 
        name: "cio"
        options:
          profile: "SNAPSHOT"
    {% endif %}